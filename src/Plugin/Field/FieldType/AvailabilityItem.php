<?php

namespace Drupal\cwiii\Plugin\Field\FieldType;

use Drupal\Core\Field\Plugin\Field\FieldType\IntegerItem;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\TypedData\DataDefinition;

/**
 * Plugin implementation of the 'worth' field type.
 *
 * @FieldType(
 *   id = "cw_availability",
 *   label = @Translation("Availability"),
 *   description = @Translation("Store 21 binary values"),
 *   default_widget = "cw_availability",
 *   default_formatter = "cw_availability"
 * )
 */
class AvailabilityItem extends IntegerItem {

  /**
   * {@inheritDoc}
   */
  public static function propertydefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('integer')
      ->setLabel(t('Availability'))
      ->setDescription(t('The week divided into 21 slots for which the user is available or not.'));
    return $properties;
  }

  /**
   * {@inheritDoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        'value' => [
          'description' => '21 digit binary number, decimalised',
          'type' => 'int',
          'size' => 'normal',
          'not null' => TRUE,
          'default' => 0,
        ]
      ]
    ];
  }

  /**
   * {@inheritDoc}
   */
  public function isEmpty() {
    return $this->value == 0;
  }

  /**
   * {@inheritDoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    return rand(0, 2097151);
  }

  public function getBinaryArray() {
    $value = (int)$this->value;
    $string = str_pad((string)decbin($value), 21, 0, STR_PAD_LEFT);
    $defaults = str_split($string);
    array_unshift($defaults, null);
    unset($defaults[0]);
    return $defaults;
  }

}
