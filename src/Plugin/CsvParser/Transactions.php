<?php

namespace Drupal\cwiii\Plugin\CsvParser;

/**
 * Plugin to import users.
 *
 * @Plugin(
 *   id = "cwiii_transactions",
 *   label = "CW3 Transactions",
 *   entity_type = "mcapi_transaction"
 * )
 */
class Transactions extends \Drupal\cforge\Plugin\CsvParser\Transactions {

  /**
   * {@inheritDoc}
   */
  public function columns() {
    $cols = parent::columns();
    unset($cols['worth.value']);
    unset($cols['category']);
    $cols += [
      'service_date' => t("A string in the form Y-m-d"),
      'quantity' => t('in the format 1.5 meaning 1 hour 30 mins'),
      'categories' => t('one category in the format "parent cat:child cat')
    ];
    return $cols;
  }

  function quantityProcess($val) {
    $this->entity->worth->curr_id = 'hr';
    $this->entity->worth->value = floor($val*60);
  }

  function service_dateProcess($val){
    $date = substr($val, 0, strpos($val, ' ')); // Remove the time of day
    $this->entity->service_date->value = $date;
  }

  function stateProcess($val) {
    return 'done';
  }

  public function categoriesProcess($val) {
    // not all transactions have child categories.
    [$parent, $child] = explode(':', $val);
    if ($parent && $child) {
      $cat = cwiii_import_category($val);
    }
    elseif ($parent and !$child) {
      $tids = \Drupal::entityQuery('taxonomy_term')
        ->condition('vid', 'categories')
        ->condition('name', $parent)
        ->execute();
      $cat = ['target_id' => reset($tids)];
    }
    $this->entity->category->setValue($cat);
  }
}
