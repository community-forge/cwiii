<?php

namespace Drupal\cwiii\Plugin\CsvParser;
use Drupal\taxonomy\Entity\Term;
use Drupal\cforge_import\Plugin\CsvParser\ImportBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Entity\EntityTypeManager;

/**
 * Plugin to import parent and child terms into cateories vocab.
 *
 * @Plugin(
 *   id = "cwiii_categories",
 *   label = "CW3 Categories",
 *   entity_type = "taxonomy_term",
 *   bundle = "categories"
 * )
 */
class Categories extends ImportBase {

  protected $termStorage;

  public function __construct($configuration, $plugin_id, $plugin_definition, $logger_channel, EntityTypeManager $entity_type_manager) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $logger_channel);
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
  }

  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('logger.channel.cforge'),
      $container->get('entity_type.manager'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function columns() {
    return [
      'parent' => t('Parent category name'),
      'child' => t('Child category name'),
    ];
  }

  protected function parentProcess($label) {
    $tid = $this->getCat($label);
    if (!$tid) {
      Term::Create(['name' => $label, 'parent' => 0, 'vid' => 'categories'])->save();
    }
  }

  protected function childProcess($label) {
    $parent_tid = $this->getCat($this->fields['parent']);
    $this->entity->set('parent', $parent_tid);
    $this->entity->set('name', $label);
  }

  private function getCat($label) : int {
    $q = $this->termStorage->getQuery()->condition('name', $label);
    $tids = $q->execute();
    return reset($tids);
  }

  public static function deleteAll() {
    $tids = \Drupal::entityQuery('taxonomy_term')->condition('vid', 'categories')->execute();
    foreach (Term::loadMultiple($tids) as $term) {
      $term->delete();
    }
  }

  function ready() : bool {
    return TRUE;
  }

}
