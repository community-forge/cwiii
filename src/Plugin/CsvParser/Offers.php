<?php

namespace Drupal\cwiii\Plugin\CsvParser;

/**
 * Plugin to import users.
 *
 * @Plugin(
 *   id = "cwiii_offers",
 *   label = "CW3 Offers",
 *   entity_type = "smallad",
 *   bundle = "offer"
 * )
 */
class Offers extends \Drupal\cforge\Plugin\CsvParser\Offers {

  public function columns() {
    $cols = parent::columns();
    unset($cols['name']);
    $cols['email'] = t('email of the owner user');
    $cols['categories'] = t('in the format "parent cat:child cat;parent cat:child cat');
    return $cols;
  }

  function emailProcess($val) {
    $uids = \Drupal::entityQuery('user')->condition('mail', trim($val))->execute();
    $this->entity->uid->target_id = reset($uids);
  }

  public function categoriesProcess($val) {
    $values = [];
    foreach (explode(';', $val) as $cat_pair) {
      $values[] = cwiii_import_category($cat_pair);
    }
    $this->entity->categories->setValue($values);
  }
}
