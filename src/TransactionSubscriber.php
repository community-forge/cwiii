<?php

namespace Drupal\cwiii;

use Drupal\mcapi\Event\McapiEvents;
use Drupal\mcapi\Event\TransactionAssembleEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * A late subscriber to add signature slots to a transaction while building it.
 */
class TransactionSubscriber implements EventSubscriberInterface {

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() : array{
    return [
      // Must run AFTER signatures are added
      McapiEvents::ASSEMBLE => ['modifySignatures', -11]
    ];
  }

  /**
   * Remove signatures from credit transactions
   *
   * @param TransactionAssembleEvent $event
   *   $event->getSubject() gives the transaction.
   */
  public function modifySignatures(TransactionAssembleEvent $event) {
    $transaction = $event->getSubject();
    if (isset($transaction->signatures)) {
      $creator_id = $transaction->creator->target_id;
      if ($creator_id == $transaction->payer->entity->getOwnerId()) { //credit
        unset($transaction->signatures);
debug('Removed signatures from credit transaction.');
        $transaction->state->setValue('done');
      }
      elseif ($creator_id == $transaction->payee->entity->getOwnerId()) { //bill
debug('Left payer signature for bill transaction');
      }
      else {// 3rd party transactions: only the payer should sign.
        $payee_uid = $transaction->payee->entity->getOwnerId();
        unset($transaction->signatures[$payee_uid]);
debug('Removed payee signature from 3rdparty transaction.');
      }
    }
  }

}
