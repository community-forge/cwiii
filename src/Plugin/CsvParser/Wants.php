<?php

namespace Drupal\cwiii\Plugin\CsvParser;

/**
 * Plugin to import users.
 *
 * @Plugin(
 *   id = "cwiii_wants",
 *   label = "CW3 Wants",
 *   entity_type = "smallad",
 *   bundle = "want"
 * )
 */
class Wants extends \Drupal\cforge\Plugin\CsvParser\Wants {

  public function columns() {
    $cols = parent::columns();
    unset($cols['name']);
    $cols['email'] = t('email of the owner user');
    $cols['categories'] = t('in the format "parent cat:child cat;parent cat:child cat');
    return $cols;
  }

  function emailProcess($val) {
    $uids = \Drupal::entityQuery('user')->condition('mail', trim($val))->execute();
    $this->entity->uid->target_id = reset($uids);
  }

  public function categoriesProcess($val) {
    $values = [];
    foreach (explode(';', $val) as $cat_pair) {
      $values[] = cwiii_import_category($cat_pair);
    }
    $this->entity->categories->setValue($values);
  }

}
