<?php

namespace Drupal\cwiii\Element;

use Drupal\Core\Render\Element\Checkboxes;
use Drupal\Core\Render\Element;
use Drupal\Core\Render\Element\Table;

/**
 * Provides a form element for choosing several relatives of the transaction.
 *
 * @FormElement("availability")
 */
class Availability extends Checkboxes {

  public function getInfo() {
    $info = parent::getInfo();
    $info['#pre_render'][]  = [static::class, 'makeTable'];
    $info['#tree'] = TRUE;
    return $info;
  }

  /**
   * Prerender callback.
   */
  public static function makeTable($element) {
    [$days, $times] = static::headers();
    $chunked = array_chunk(Element::children($element), 3);
    $boxes = array_map(NULL, ...$chunked);
    $new_element['#theme'] = 'table';
    $new_element['#header'] = [-1 => '']+$days;
    foreach ($boxes as $row_of_boxes) {// 7 boxes
      $time = (string)array_shift($times);
      $row = ['header' => $time];
      foreach ($row_of_boxes as $delta => $box) {
        $row[(string)$days[$delta]] = ['data' => $element[$box]];
      }
      $new_element['#rows'][$time] = $row;
    }
    Table::preRenderTable($new_element);

    return $new_element;
  }

  public static function headers() {
    $days = [t('Su'), t('Mo'), t('Tu'), t('We'), t('Th'), t('Fr'), t('Sa')];
    $times = [t('Morning'), t('Afternoon'), t('Evening')];
    return [$days, $times];
  }

}

