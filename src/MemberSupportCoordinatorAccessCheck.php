<?php

namespace Drupal\cwiii;

use Drupal\Core\Entity\EntityAccessCheck;
use Symfony\Component\Routing\Route;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Access\AccessResult;
use Drupal\user\Entity\User;

/**
 * Class to grant access to the Member support coordinator
 */

class MemberSupportCoordinatorAccessCheck extends EntityAccessCheck {

  /**
   * Prevent access to specific nodes and menu items
   * @param Route $route
   * @param RouteMatchInterface $route_match
   * @param AccountInterface $account
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access(Route $route, RouteMatchInterface $route_match, AccountInterface $account) {
    $account = User::load($account->id());
    if ($account->hasPermission('administer users') or $account->hasRole('member_support_coordinator')) {
      return AccessResult::allowed();
    }
    return AccessResult::forbidden();
  }
}

