<?php

namespace Drupal\cwiii\Plugin\CsvParser;
use Drupal\taxonomy\Entity\Term;
use Drupal\field\Entity\FieldConfig;
use Drupal\cforge\Plugin\Field\FieldType\ContactMeItem;

/**
 * Plugin to import users.
 *
 * @Plugin(
 *   id = "cwiii_users",
 *   label = "CW3 Users",
 *   entity_type = "user"
 * )
 */
class Users extends \Drupal\cforge\Plugin\CsvParser\Users {

  /**
   * {@inheritDoc}
   */
  public function columns() {
    $cols = parent::columns();
    foreach (ContactMeItem::CHANNELS as $sm => $info) {
      unset($cols['contactme.'.$sm]);
    }
    return $cols + [
      'tags' => 'Tags for admins, semicolon separated',
      'service_category' => 'Categories in the format "Parent Name:Child name;"',
      'availability' => 'Binary string 21 bits long.',
      'contactme' => t('Aggregated ContactMe fields, with CW headings'),
      'org_name' => t('Organisation name (to be shifted to username)'),
      'birthday' => t('Birthday in any format known to php strtotime')
    ];
  }

  /**
   * {@inheritDoc}
   */
  public static function deleteAll() {
    parent::deleteAll();
    $tags = \Drupal::entityTypeManager()
      ->getStorage('taxonomy_term')
      ->loadByProperties(['vid' => 'user_tags']);
    foreach ($tags as $tag) {
      $tag->delete();
    }
  }

  /**
   * {@inheritDoc}
   *
   * Disable the contactMe field required while doing import.
   */
  public static function saveEntities(string $plugin_id, array $rows, bool $save, array &$sandbox = []) : void {
    $field = FieldConfig::load('user.user.contactme');
    $field->setRequired(FALSE)->save();
    parent::saveEntities($plugin_id, $rows, $save, $sandbox);
    $field->setRequired(TRUE)->save();
  }

  /**
   * Preprocessing for csv field 'service_category'.
   */
  public function service_categoryProcess($val) {
    $values = [];
    foreach (explode(';', $val) as $cat_pair) {
      $values[] = cwiii_import_category($cat_pair);
    }
    $this->entity->skills->setValue($values);
  }

  /**
   * Preprocessing for csv field 'tags'.
   */
  protected function tagsProcess($val) {
    if (empty($val) or $val == 'NULL') return;
    foreach (explode(';', $val) as $term_name) {
      $existing = \Drupal::entityTypeManager()
        ->getStorage('taxonomy_term')
        ->loadByProperties(['name' => $term_name]);
      if (empty($existing)) {
        $term = Term::Create(['name' => $term_name, 'vid' => 'user_tags']);
        $term->save();
        $terms[] = ['target_id' => $term->id()];
      }
      else {
        //debug("Reusing user tag $term_name: ".reset($existing)->id());
        $terms[] = ['target_id' => reset($existing)->id()];
      }
    }
    $this->entity->tags->setValue($terms);
  }

  /**
   * Preprocessing for csv field 'availability'.
   */
  protected function availabilityProcess($val) {
    $val = str_pad($val, 21, '0', STR_PAD_LEFT);// correct dodgy spreadsheet save settings.
    $value = preg_match('/[01]{21}/', $val) ? bindec($val) : 0;
    $this->entity->availability->setValue($value);
  }


  /**
   * Preprocessing for csv field 'cw_contact'.
   */
  protected function contactmeProcess($val) {
    foreach (explode('|', $val) as $subfield) {
      [$cwfield, $handle] = explode(':', $subfield);
      if ($handle = trim($handle)) {
        switch ($cwfield) {
          // Note that these might overwrite each other when several values feed into one.
          case 'Phone (Cell)':
            $prop = 'mob'; break;
          case 'Phone':
          case 'Phone (Home)':
          case 'Phone (Work)':
            $prop = 'tel'; break;
          case 'Alternate Email':
          case 'Alternate Email (Home)':
          case 'Alternate Email (Work)':
            $prop = 'altmail'; break;
          case 'Twitter':
          case 'Facebook':
          case 'LinkedIn':
            $prop = strtolower($cwfield); break;
          case 'Web Address':
            $prop = 'url'; break;
          default:
            die('Invalid contact field: '.$cwfield);
        }
        $this->entity->contactme->{$prop} = $handle;
      }
    }
  }

  /**
   * Preprocessing for csv field 'org_name'.
   */
  function org_nameProcess($val) {
    if ($val and $val <> 'NULL') {
      $this->entity->address->given_name = $val;
    }
  }

  /**
   * Preprocessing for csv field 'birthday'.
   */
  public function birthday($val) {
    if($val) {
      $unixtime = strtotime($val);
      $this->entity->birthday->value = date('Y-m-d', $unixtime);
    }
  }

  /**
   * {@inheritDoc}
   * Ready only after categories have been imported.
   */
  public function ready() : bool {
    // There must be more than category with parents
    $cats = \Drupal::entityQuery('taxonomy_term')
      ->condition('vid', 'categories')
      ->condition('parent', 0, '<>')
      ->execute();
    return (bool) $cats;
  }

}