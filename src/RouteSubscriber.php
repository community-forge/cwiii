<?php

namespace Drupal\cwiii;

use Drupal\Core\Routing\RouteSubscriberBase;
use Symfony\Component\Routing\RouteCollection;

/**
 *
 */
class RouteSubscriber extends RouteSubscriberBase {

  protected function alterRoutes(RouteCollection $collection) {
    $collection->get('entity.user.admin')
      ->setRequirements(['_member_support_coordinator' => 'true']);
  }
}
