<?php

namespace Drupal\cwiii\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\WidgetBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'worth' widget.
 *
 * @FieldWidget(
 *   id = "cw_availability",
 *   label = @Translation("Availability"),
 *   field_types = {
 *     "cw_availability"
 *   },
 *   multiple_values = false
 * )
 */
class AvailabilityWidget extends WidgetBase {

  /**
   * Returns the form for a single field widget.
   *
   * @see \Drupal\Core\Field\WidgetInterface
   */
  public function formElement(FieldItemListInterface $items, $delta, array $element, array &$form, FormStateInterface $form_state) {
    $element = [
      '#type' => 'availability',
      '#options' => [],
      '#default_value' => []
    ];
    $defaults = $items->first()->getBinaryArray();
    foreach ($defaults as $key => $bin) {
      $element['#options'][$key] = '';
      if ($defaults[$key]) {
        $element['#default_value'][] = $key;
      }
    }
    return $element;
  }

  function massageFormValues(array $values, array $form, FormStateInterface $form_state): array {
    $string = 0;
    for($i=1;$i<=21;$i++) {
      $string .= empty($values[0][$i]) ? 0 : 1;
    }
    return ['value' => bindec($string)];
  }

  /**
   * {@inheritDoc}
   */
//  public function errorElement(array $element, ConstraintViolationInterface $violation, array $form, FormStateInterface $form_state) {
//    list($key) = explode('.', $violation->getpropertyPath());
//    return $element[$key];
//  }


  /**
   * {@inheritDoc}
   */
//  public function settingsSummary() {
//    // count
//    return [
//      '#markup' => $this->t(
//        '@count/@total channels active.',
//        [
//          '@count' => count(array_filter($this->getSetting('channels'))),
//          '@total' => 12
//        ]
//      )
//    ];
//  }

}
