<?php

namespace Drupal\cwiii\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldFormatter\BooleanFormatter;

/**
 * Plugin implementation of the 'author' formatter.
 *
 * @FieldFormatter(
 *   id = "cw_availability",
 *   label = @Translation("Availability"),
 *   description = @Translation("A weekly grid showing availability in morning, afternoon and evening."),
 *   field_types = {
 *     "cw_availability"
 *   }
 * )
 */
class AvailabilityFormatter extends BooleanFormatter {

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $format = $this->getSetting('format');
    if ($format == 'default') {
      $format = 'unicode-yes-no';
    }
    $options = $this->getOutputFormats()[$format];
    [$days, $times] = \Drupal\cwiii\Element\Availability::headers();
    foreach ($items as $delta => $item) {
      $elements[$delta] = [
        '#type' => 'table',
        '#header' => array_merge([''], $days)
      ];

      $defaults = $item->getBinaryArray();
      $grid = array_chunk($defaults, 3);

      $grid = array_map(NULL, ...$grid);// flip the grid
      foreach ($grid as $time_values) {
        $timeofDay = (string)array_shift($times);
        $elements[$delta]['#rows'][$timeofDay]['heading']['data'] = $timeofDay;
        foreach ($time_values as $key => $dayValue) {
          $elements[$delta]['#rows'][$timeofDay][$key]['data'] = $options[!$dayValue];
        }
      }
    }
    return $elements;
  }

}
